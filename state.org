#+begin_export latex
\tikzset{
node distance=3cm, % specifies the minimum distance between two nodes. Change if necessary.
every state/.style={semithick, fill=gray!10}, % sets the properties for each 'state' node
initial text={},% sets the text that appears on the start arrow
every edge/.style={ %
draw,
->,         % makes the edges directed
>=stealth', % with arrow heads bold
auto,
semithick,
%sep=2pt,
bend angle=15
}
}
#+end_export
