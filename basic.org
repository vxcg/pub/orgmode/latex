#+Latex_Header: \usepackage{import}
#+Latex_Header: \subimport{../themes/math/org-templates/}{preamble}
#+Latex_Header: \subimport{../themes/math/org-templates/}{math}
#+Latex_Header: \mathchardef\mhyphen="2D
#+latex_header: \usepackage{tikz}
#+LaTeX_HEADER: \usepackage{tikz-qtree}
#+latex_header: \usetikzlibrary{automata, positioning, arrows, shapes.geometric}
# for sout (strikethrough) 
#+LaTeX_HEADER: \usepackage{ulem} 
#+LaTeX_HEADER: \newcommand{\stkout}[1]{\ifmmode\text{\sout{\ensuremath{#1}}}\else\sout{#1}\fi}


#+LATEX_CLASS: article
#+LATEX_CLASS_OPTIONS:
#+LATEX_HEADER:
#+LATEX_HEADER_EXTRA:
#+DESCRIPTION:
#+KEYWORDS:
#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t
#+OPTIONS: arch:headline author:t broken-links:nil c:nil
#+OPTIONS: creator:nil d:(not "LOGBOOK") date:t e:t
#+OPTIONS: email:nil f:t inline:t num:t p:nil pri:nil
#+OPTIONS: prop:nil stat:t tags:t tasks:t tex:t timestamp:t
#+OPTIONS: title:t toc:nil todo:t |:t




